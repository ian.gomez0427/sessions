// inserting multiple documents at once
// "":"",

db.users.insertMany([
	{
		"firstName": "John",
		"lastName": "Doe"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe"
	}
	]);

db.users.insertMany([
{
"firstName": "Ian",
"lastName": "Gomez"
},
{
"firstName": "Theo",
"lastName": "Gomez"
}
]);

// retrieving all the inserted users
db.users.find();

// retrieving a specific document from a collection
db.users.find({"firstName": "Ian"});

// [SECTION] updating existing documents
db.users.updateOne(
	{
		"_id": "64c1c59c2ddab7f66eff6211"
	},
	{
		$set:{"lastName": "Pogi"}
}
);

// updating multiple documents
db.users.updateMany(
{
	"lastName": "Doe"
},
{
	$set:{"firstName": "Mary"}
}
);

// [SECTION] deleting documents from a collection
db.users.deleteMany({ "lastName": "Doe" });
db.users.deleteOne({
    "_id": ObjectId("64c1c45f98a5baa752634263")
});

db.users.updateMany(
  { firstName: "Ian" },
  { $rename: { "lastname": "lastName" } }
);