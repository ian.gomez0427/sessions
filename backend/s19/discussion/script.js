// Javascript is consisted of 'statements'. Statements are basically the syntax with semicolons at the end
// alert("Hello World!");

// Javascript can access the 'log' function of the console to display text/data in the console.
console.log("Hello World!")

// [SECTION] Variables

// Variable Declaration & Invocation
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings
let country = "Philippines"
let province = "Metro Manila"

let full_address = province + ", " + country;
console.log(full_address);
console.log(province + ", " + country);

// Numbers/ Integers
let headcount = 26;
let grade = 98.7;

console.log("The number of students is "+headcount+" and the average grade of all students is " + grade);

let total=headcount+grade;
console.log(total);

// Boolean - value is either TRUE or FALSE
let is_married = false;
let is_good_conduct = true;

console.log("He's married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);

// Objects
let person={
	full_name: "Ian Gomez",
	age: 25,
	isMarried: false,
	contact: ["09231234567", "09990000000"],
	address:{
		house_number: "345",
		city: "England"
	}
}

// javascript reads arrays as objects. This is to accomodate specific functionalities that arrays can do later on.
console.log(typeof person);
console.log(typeof grades);
console.log(typeof is_married);
console.log(typeof total);

// Null & undefined
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators

// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Difference: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);

// Assignment Operators
let assignment_number=0;

assignment_number = assignment_number + 2;
assignment_number +=2;

console.log("Result of addition assignment operator: " + assignment_number);
console.log("Result of shorthand addition assignment operator: " +assignment_number);
