// FUNCTION DECLARATION & INVOCATION

function printName(){
	console.log("My name is Ian");
}

printName();

let variable_function = function(){
	console.log("Hello from function expression!")
}

variable_function();

// SCOPING

let global_variable = "Call me Mr. Worldwide";
console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

console.log(function_variable);
console.log(function_const);

// can use global variable inside a function
console.log(global_variable);
}

/* cannot use locally scoped variable outside the function
console.log(function_variable); */

showNames();

// NESTED FUNCTIONS

function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		console.log(name);
		
		console.log(nested_name);
	
	}

	childFunction();
	/*not working because outside the child-function
	console.log(nested_name);
	*/
}

parentFunction();

// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser(){
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, "+ first_name+ " "+ last_name+ "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();

// RETURN STATEMENT

function fullName(){
	return "Ian Gomez";
}
console.log(fullName());


function returnFullName(){
	console.log("Ian Gomez");
}
returnFullName();
