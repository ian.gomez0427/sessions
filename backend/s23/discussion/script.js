// [SECTION] IF-ELSE STATEMENTS

let number = 1;

if (number >1){
	console.log("The number is greater than 1.");
} else if (number < 1){
	console.log("The number is less than 1.");
}
else{
	console.log("The number is equal to 1.");
}

// FALSEY VALUES
// will not execute because the condition is false

if (false){
	console.log("Falsey");
}

if (0){
	console.log("Falsey");
}

if (undefined){
	console.log("Falsey");
}

if (null){
	console.log("Falsey");
}

// TRUTHY VALUES
// will execute because the conditions are True

if (true){
	console.log("Truthy");
}

if (1){
	console.log("Truthy");
}

if ([]){
	console.log("Truthy");
}

// TERNARY OPERATORS

let result = (1 < 10)? true:false
console.log("Value returned from the ternary operator is " +result);
/* 
This is the if-else equivalent of the ternary operation above.

if(1<10){
	return true;
	else{
		return false
	}
}
*/


let check = (1 < 10)? "tama ka boy":"mali yan boy";
console.log("Value returned from the ternary operator is " +check);

if(5==5){
	let greeting = "hello";
	console.log(greeting);
}
console.log("Value returned from the ternary operator is " +result);


// [SECTION] SWITCH STATEMENTS

/*
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case 'monday':
		console.log("The day today is Monday!");
		break;

	case 'tuesday':
		console.log("The day today is Tuesday!");
		break;

	case 'wednesday':
		console.log("The day today is Wednesday!");
		break;

	case 'thursday':
		console.log("The day today is Thursday!");
		break;

	case 'friday':
		console.log("The day today is Friday!");
		break;

	case 'saturday':
		console.log("The day today is Friday!");
		break;
		
	case 'sunday':
		console.log("The day today is Friday!");
		break;	

	default:
		console.log("Please input a valid day naman pre!");
		break;
}
*/

// [SECTION] Try/Catch/Finally Statements

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch(error){
		console.log(error.message);
	}finally{
		alert("Intensity updates will show new alert!");
	}
}
showIntensityAlert(56);