
// // [SECTION] While Loop

// let count = 5;

// // let count = prompt("Enter a number: ");

// while (count !== 0){
// 	console.log("Current value of count: " +count);
// 	count--;
// }


// // [SECTION] Do-While Loop

// /*Number converts user input to number data type*/

// // let number = Number(prompt("Give me a number: "));
// let number = 0;

// do{
// 	console.log("Current Value of number: " +number);
// 	number+=1;
// } while (number <= 10)


// // [SECTION] For Loop

// /*
// for (let count= prompt("Enter a number: "); count <=20; count++){
// 	console.log("Current for loop value: " +count);
// }
// */

// for (let count= 15; count <=20; count++){
// 	console.log("Current for loop value: " +count);
// }
// ////////////////////////////////////
// let my_string = "Ian Gomez";

// console.log(my_string.length); //To get the length of the string
// console.log(my_string[4]) // To get a specific letter
// ////////////////////////////////////

// // Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// for (let index=0; index < my_string.length; index++){
// 	console.log(my_string[index]);
// }


// MINI ACTIVITY - filtering through data
/*
1. Loop through the 'my_name' variable which has a string with your name on it.
2. Display each letter in the console but exclude all the vowels from it
*/

// let my_name = "Matteo Tristan Gomez";
// for (let index=0; index < my_name.length; index++){
// 	if(my_name[index].toLowerCase()== "a"||
// 		my_name[index].toLowerCase()== "e"||
// 		my_name[index].toLowerCase()== "i"||
// 		my_name[index].toLowerCase()== "o"||
// 		my_name[index].toLowerCase()== "u"
// 	){
// 		continue
// 	}else {
// 		console.log(my_name[index]);
// 	}
// } 

// not working yet
// let my_name1 = "Matteo Tristan Gomez";
// for (let index=0; index < my_name1.length; index++){
// 	if(my_name1[index].toLowerCase()!= "a"||
// 		my_name1[index].toLowerCase()!= "e"||
// 		my_name1[index].toLowerCase()!= "i"||
// 		my_name1[index].toLowerCase()!= "o"||
// 		my_name1[index].toLowerCase()!= "u"
// 	){
// 		console.log(my_name[index]);
// 	}
// } 


// let name_two = "rafael";

// for (let index=0; index < name_two.length; index++){
// 	console.log(name_two[index]);

// if (name_two[index].toLowerCase()=="a"){
// 	console.log("Skipping...");
// 	continue
// }

// if(name_two[index]=="e"){
// 	break;
// }
// }

let name_two = "rafael";

for (let index=0; index < name_two.length; index++){

if (name_two[index].toLowerCase()=="a"){
	console.log("Skipping...");
	continue
}

if(name_two[index]=="e"){
	break;
}console.log(name_two[index]);
}