// console.log("love, objects");

// [SECTION] OBJECTS
/*	- an Object is a data type to represent real world objects
	- create properties and methods/ functionalities
*/

// Creating objects using initializers/ object literals {}

let cellphone = {
	// key:value pair
	name: "Nokia 3210",
	manufacturerDate: 1999
};
console.log("Result from creating objects using initializers/ object literals");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
function Laptop (name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
// multiple instance of an object using the "new" keyword
// this method is called instantiation
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating an object using constructor function");
console.log(laptop);

let laptop2 = new Laptop("Macbook Air", 2020);
console.log("Result from creating an object using constructor function");
console.log(laptop2);

// [SECTION] Accessing Object Properties

// using [square bracket] notation
console.log("Result from [square bracket] notation: " + laptop2["name"]);
console.log("Result from [square bracket] notation: " + laptop2["manufactureDate"]);

// using .dot notation
console.log("Result from .dot notation: " +laptop2.name);
console.log("Result from .dot notation: " +laptop2.manufactureDate);

// access Array Objects
let array = [laptop, laptop2];

console.log(array[0]["name"]);
console.log(array[0].name);

// [SECTION] Adding/Deleting/Reassigning Object Properties

// empty object using object literal
let car = {};
// empty object using constructor function/instantiation
let myCar = new Object()

// adding object properties
car.name = "Honda Civic";
console.log("Result from adding properties using .dot notation: ");
console.log(car);
car.color = "black";
console.log(car);

// adding object properties using the [square bracket] notation
car["manufacturing date"] = 2019;
console.log(car["manufacturing date"]);
// console.log(car["manufacturing Date"]);
// console.log(car.manufacturing date); //we cannot access object property using dot notation if the key has spaces
console.log("Result from adding properties using square bracket notation: ");
console.log(car);

// Deleting Object Properties
delete car["manufacturing date"];
delete car.color;
console.log("Result from deleting properties: ");
console.log(car);

// reassigning object properties
car.name = "Honda Civic Type R";
console.log("Result from reassigning properties: ");
console.log(car);

// [SECTION] Object Methods
/*
 - a method is a function which acts as a property of an object
*/

let person = {
	name: "Barbie",
	gender: "female",
	greet: function(){
		// console.log("Hello! My name is" +person.name);
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person);
console.log("Name: " +person.name);
console.log("Gender: " +person.gender);
console.log("Result from object methods");
//greet() is now called a method
person.greet();

// adding methods to objects
person.walk = function(){
	console.log(this.name + " walked 25 steps forward")
}
person.walk();

let friend = {
	// values can be string, number, arrays, object
	name: "Ken",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ["ken@gmail.com", "ken@mail.com"],
	introduce: function(object){
		console.log("Nice to meet you " +person.name + ". I am " +this.name + " from " +this.address.city + " " + this.address.state);
	}
}
console.log(friend);
friend.introduce(person);

