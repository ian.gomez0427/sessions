// [SECTION] Arrays and Indexes

console.log("[SECTION] Arrays and Indexes");
console.log("");
let grades= [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative Way to write arrays

let my_tasks = [
	"drink HTML",
	"eat Javascript",
	"inhale CSS",
	"bake sass"
];

// Reassigning Values 
console.log("Array before re-assignment");
console.log(my_tasks);

my_tasks[0] = "run Hello World";
console.log("Array after re-assignment");
console.log(my_tasks);


// [SECTION] Reading from Arrays
console.log("");
console.log("Reading from Arrays");
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an array
console.log("");
console.log("Getting the length of an array");
console.log(computer_brands.length);

// Accessing last element in an array
console.log("");
console.log("Accessing last element in an array");
let index_of_last_element = computer_brands.length -1;
console.log (computer_brands[index_of_last_element]);

// [SECTION] Array Methods - Mutator Array Methods - makes changes on an array
// Push method - adds items at the end of the array
console.log("");
console.log("[SECTION] Array Methods");
console.log("");
console.log("Push method");
console.log("");
let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];

console.log("Current Array");
console.log(fruits);

fruits.push("Mango", "Cocomelon", [1,2,3,4]);

console.log("Updated array after push method");
console.log(fruits);

// Pop method - removes the last element of an array
console.log("");
console.log("Pop method");
console.log("");

console.log("Current Array");
console.log(fruits);

fruits.pop();
let removed_item = fruits.pop();

console.log("Updated array after pop method");
console.log(fruits);
console.log("Removed fruit :" + removed_item);

// Unshift Method - adds elements in the beginning of the array
console.log("");
console.log("Unshift Method");
console.log("");

console.log("Current Array");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method");
console.log(fruits);

// Shift Method - removes element from the beginning of the array
console.log("");
console.log("Shift Method");
console.log("");

console.log("Current Array");
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method");
console.log(fruits);

// Splice Method - updates elements in an array starting from a specified index
console.log("");
console.log("Splice Method");
console.log("");

console.log("Current Array");
console.log(fruits);

// splice (startingIndex, numberOfItemsToBeReplaced, itemsToBeAdded)
fruits.splice(1,2,"Lime", "Cherry"); 

console.log("Updated array after Splice method");
console.log(fruits);

// Sort Method - arranges array elements alphabetically
// https://www.w3schools.com/jsref/jsref_sort.asp
console.log("");
console.log("Sort Method");
console.log("");

console.log("Current Array");
console.log(fruits);

fruits.sort(); //alphabetical

console.log("Updated array after Sort method");
console.log(fruits);

fruits.reverse(); //reversed alphabetical
console.log(fruits);

// [Sub-section] Non-Mutator Methods - execute specific functionalities in an array

console.log("");
console.log("[Sub-section] Non-Mutator Methods");
console.log("");

// indexOf - gets the index of a specific item from an array
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of 1st lenovo is: " +index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is: " +index_of_lenovo_from_last_item);

// Slice Method 
console.log("");
console.log("Sliced Method");
let hobbies = ["gaming", "running", "cheating", "cycling", "writing"];

let sliced_array_from_hobbies = hobbies.slice(2); //cut the array starting from the specified index [2]
console.log(hobbies);
console.log(sliced_array_from_hobbies);

let sliced_array_from_hobbies_B = hobbies.slice(1, 3); //cut the array starting from index [2] and end(do not include) index [3]
console.log(sliced_array_from_hobbies_B);

let sliced_array_from_hobbies_C = hobbies.slice(-3); //cut the array starting from the end of the array (last item starts at [-1])
console.log(sliced_array_from_hobbies_C);

// toString Method

console.log("");
console.log("toString Method");

let string_array = hobbies.toString();
console.log(string_array);

// concat Method - combines two arrays

console.log("");
console.log("concat Method");

let greeting = ["hello", "world"];
let exclamation = ["!", "?"];
let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

// join method - converts array to string and inserts specified separator between them defined as the argument of the join () function.
console.log("");
console.log("join Method");
console.log(hobbies);
console.log(hobbies.join(" @ "));

//forEach Method
console.log("");
console.log("forEach Method");

hobbies.forEach(function(hobby){
	console.log(hobby);
})

// map Method - loops throughout the whole array and adds each item to a new array.
console.log("");
console.log("map Method");

let numbers_list = [1,2,3,4,5];
let numbers_map = numbers_list.map(function(number){
	return number * 2;
});
console.log("Original numbers_list: ");
console.log(numbers_list);
console.log("Array after numbers_map: ");
console.log(numbers_map);

// filter Method - 
console.log("");
console.log("filter Method");

let filtered_numbers = numbers_list.filter(function(number){
	return(number <3);
})
console.log(filtered_numbers);

// [SECTION] Multi - Dimensional Arrays
console.log("");
console.log("[SECTION] Multi - Dimensional Arrays");

let chess_board = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log(chess_board[1][4]);
