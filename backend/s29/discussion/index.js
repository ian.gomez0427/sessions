// console.log("Hello DOM");
// Get post data - to retrieve data from jsonplaceholder API.
// Promise chain

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
	// console.log(data)
	showPosts(data)
});

console.log(document)
console.log(document.querySelector("#form-add-post"))
let textTitle = document.querySelector("#txt-title").value
console.log(textTitle);
console.log(document.querySelector("#txt-body").value)

// Add Post Data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	// prevent the default behavior of our form element
	e.preventDefault();
	// function code block or task after trigerring the event.
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST", 
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
	console.log(data);
	alert("Post Successfully created.");

	document.querySelector("#txt-title").value=null;
	document.querySelector("#txt-body").value=null;
	})

});

// Show post - to display all the posts objects from our jsonplaceholder API.
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach((post) => {
		// JSX Syntax - Javascript Extended / Extension
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick = "deletePost(${post.id}
					")">Delete</button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;

};

// Edit Post form.
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// Update Button Function
// Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post Updated successfully!")
	})

	document.querySelector("#txt-edit-id").value = null;
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;
	document.querySelector("#btn-submit-update").setAttribute("disabled", true);
})

// Function to delete all posts
const deleteAllPosts = () => {
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'DELETE',
  })
    .then((response) => response.json())
    .then(() => {
      alert('All posts deleted successfully!');
      showPosts([]); // Clear the post entries on the page after deletion
    });
};

// Event listener for the "Delete All" button
document.querySelector('#delete-all').addEventListener('click', () => {
  if (confirm('Are you sure you want to delete all posts?')) {
    deleteAllPosts();
  }
});

// Delete Post function
const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'DELETE',
  })
    .then((response) => {
      if (response.ok) {
        document.querySelector(`#post-${id}`).remove();
        alert('Post deleted successfully!');
      } else {
        alert('Failed to delete post. Please try again.');
      }
    });
};

// Event delegation for delete buttons (since they are dynamically created)
document.addEventListener('click', (e) => {
  if (e.target && e.target.matches('.delete-btn')) {
    const postId = e.target.dataset.postId;
    if (confirm('Are you sure you want to delete this post?')) {
      deletePost(postId);
    }
  }
});


// // Show post - to display all the posts objects from our jsonplaceholder API.
// const showPosts = (posts) => {
//   let postEntries = '';
//   posts.forEach((post) => {
//     // JSX Syntax - Javascript Extended / Extension
//     postEntries += `
//       <div id="post-${post.id}">
//         <h3 id="post-title-${post.id}">${post.title}</h3>
//         <p id="post-body-${post.id}">${post.body}</p>
//         <button class="edit-btn" onclick="editPost(${post.id})">Edit</button>
//         <button class="delete-btn" data-postId="${post.id}">Delete</button>
//       </div>
//     `;
//   });

  // document.querySelector('#div-post-entries').innerHTML = postEntries;
// };
