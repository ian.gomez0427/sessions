db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
            courses: [ "CSS", "Javascript", "Python" ],
            department: "none"
        },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
            courses: [ "Python", "React", "PHP" ],
            department: "none"
        },
        {
            firstName: "Neil",
            lastName: "Armstrong",
            age: 82,
            contact: {
                phone: "87654321",
                email: "neilarmstrong@gmail.com"
            },
            courses: [ "React", "Laravel", "Sass" ],
            department: "none"
        }
]);


// less than operator
db.users.find({
    age: {
        $lte: 82
    }
})

// regex operator
db.users.find({
firstName: {
$regex: 's',
$options: 'i'
}
})

// field projection
db.users.find({},{
    "_id":0
})

db.users.find({},{
    "_id":0,
    "firstName":1,
    "lastName":1
})

db.users.insertOne({
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
            courses: [ "Python", "React", "PHP" ],
            department: "none"
    })

db.users.insertOne({
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact:{
        phone: "67858969",
        email: "ashbdxjkas@mail.com"
    }
})