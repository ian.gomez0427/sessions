// using aggregate method

// match is similar to find(). 
db.fruits.aggregate([
	{$match: {onSale: true}},

	// group - allows us to group together documents and create an analysis out of the group elements
	// _id: $supplier_id
	/*
	Apple = 1.0
	Kiwi = 1.0
	Banana = 2.0

	_id: 1.0
		Apple, Kiwi

		total: sum of the fruit stock of 1.0
		total: apple stocks + kiwi stocks
		total: 20 + 25 = 45

	_id:2.0	
		Banana

		total: sum of the fruit stock of 2.0
		total: banana stocks
		total: 15
	*/ $sum - used to add or total the values in a given field
  {$group: {_id: "$supplier_id", total: {$sum:"$stock"}}}
]);

// query result
/*
{
  _id: 2,
  total: 15
}
{
  _id: 1,
  total: 45
}
*/


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avgStocks: {$avg: "$stock"}}},
	// $project - can be used when "aggregating" data to include/ exclude fields from the returned results(field projection)
	{$project:{_id: 0}}
	]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},

	{$sort: {maxPrice: -1}}
	]);

// sort by name only
db.fruits.aggregate([
  { $sort: { name: 1 } },
  { $project: { _id: 0, name: 1 } }
]);

db.fruits.aggregate([
	// $unwind - deconstruct the array field from a collection with an array value to output result for each element
	{$unwind: "$origin"}
	]);


db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id:"$origin", kinds: {$sum:1}}}
]);

db.fruits.aggregate([
	{$group: {_id:"$origin", kinds: {$sum:1}}}
]);