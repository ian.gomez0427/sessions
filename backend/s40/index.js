// Server variables for initialization
const express = require('express'); //imports express
const app = express(); // initializes express
const port = 4000;

// Middleware
app.use(express.json()); //registering a middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({extended: true})); // a middleware that will allow express to be able to read data types other than the default string and array it can ususlly read

// Server listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

module.exports = app;


//[SECTION] Routes
app.get('/', (request, response) => {
	response.send('Hello World!');
})

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}.`);
})

// mock database
let users = [];
app.post('/register', (request, response) =>{
	if (request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);

		response.send(`User ${request.body.username} has successfully been registered!`);
	} else {
		response.send ('Please input BOTH username AND password.');
	}

})

// gets the list / array of users
app.get('/users', (request, response) => {
	response.send(users);
})

// validate username and/or password is not empty
app.post('/validate', (request, response) =>{
	if (request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} has successfully been registered!`);
	}else if (request.body.username == '' && request.body.password == ''){
		response.send ('Please input BOTH username AND password.');
	} else if (request.body.username == ''){
		response.send ('Username is required.');
	} else if (request.body.password == ''){
		response.send ('Password is required.');
	}else{
		response.send ('Please input BOTH username AND password.');
	}

})

module.exports = app;

