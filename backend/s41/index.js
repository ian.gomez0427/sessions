// server variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() // initialization of dotenv package
const app = express();
const port = 4000;

// mongoDb connection
// ${process.env.MONGODD_PASSWORD}
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gomez.k6bt02u.mongodb.net/b303-todo?retryWrites=true&w=majority` , {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('Connection Error :( '));
database.once('open', () => console.log('Connected to MongoDB! '));

// middleware

app.use(express.json())
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

//[SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Routes

// creating a new task
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {

		// checks if task already exist
		if (result != null && result.name == request.body.name) {
			return response.send ("Duplicate task found!");

		// steps to create the new task
		} else {

			// 1. create a new instance of the task model which will contain that properties required based on the schema
			let newTask = new Task({
				name: request.body.name
			});

			// 2. save new task to the database
			newTask.save().then((savedTask, error) => {
				if(error) {
					return response.send({
						message: error.message
					});
				}
				return response.send(201, 'New task created!');
			})
		}
	})
})

// get all tasks
app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.status (200).json ({
			task: result
		})
	})
})

// Server listening
app.listen(port, () => console.log(`Server is running at localhost ${port}`));

module.exports = app;