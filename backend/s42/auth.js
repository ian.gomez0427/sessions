const jwt = require('jsonwebtoken');

const secret_key = "CourseBookingAPIB303";

// generating a token
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	// the jwt.sign(generates a token using user_data and secret_key. The 3rd argument serves as additional option
	return jwt.sign(user_data, secret_key, {});
}