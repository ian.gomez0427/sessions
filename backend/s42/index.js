// connection to the server

// server variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config(); // initialization of dotenv package
const taskRoutes = require('./routes/taskRoutes.js');
const app = express();
const port = 4000;

// mongoDb connection
// ${process.env.MONGODD_PASSWORD}
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gomez.k6bt02u.mongodb.net/b303-todo?retryWrites=true&w=majority` , {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('Connection Error :( '));
database.once('open', () => console.log('Connected to MongoDB! '));

// middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use('/api/tasks', taskRoutes); //initializing the routes for '/tasks' so the server will know the routes available to send requests to.

// http://localhost:4000/api/tasks/ GET method will access getAllTasks
// http://localhost:4000/api/tasks/ POST method will access createTask

// Server listening
app.listen(port, () => console.log(`Server is running at localhost ${port}`));

module.exports = app;