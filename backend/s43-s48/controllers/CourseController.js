const Course = require("../models/Course.js");

module.exports.addCourse = (request, response) => {
  let new_course = new Course({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price
  });

  return new_course.save().then((saved_course, error) => {
    if(error){
      return response.send(false);
    }

    return response.send(true);
  }).catch(error => response.send(error));
}


module.exports.getAllCourses = (request, response) => {
  return Course.find({}).then(result => {
    return response.send(result);
  })
}

module.exports.getAllActiveCourses = (request, response) => {
  return Course.find({isActive: true}).then (result => {
    return response.send(result);
  })
}

module.exports.getCourse = (request, response) => {
  return Course.findById(request.params.id).then(result => {

    return response.send(result);
  }).catch(error => {
            console.error(error);
            // return response.status(500).json({ error: 'Course not found' });
            return response.status(500).send('Course not found');
        });

}

module.exports.updateCourse = (request, response) => {
  let updated_course_details = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price
  };

  return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course,error) => {
    if (error){
      return response.send({
        message: error.message
      })
    }
    return response.send({
      message: 'Course has been updated successfully!'
    })
  })
}

module.exports.archiveCourse = (request, response) => {
    return Course.findByIdAndUpdate(request.params.id, { isActive: 'false' }).then((course,error) => {
    if (error){
      return response.send({
      message: 'Failed to Archive!'
    })
    }
    return response.send({
      message: 'Course has been archived successfully!'
    })
  })
}

module.exports.activateCourse = (request, response) => {
    return Course.findByIdAndUpdate(request.params.id, { isActive: 'true' }).then((course,error) => {
    if (error){
      return response.send({
      message: 'Failed to Activate!'
    })
    }
    return response.send({
      message: 'Course has been activated successfully!'
    })
  })
}


//////////////////////////
module.exports.searchCoursesByName = (request, response) => {
  const { courseName } = request.body;
  
  Course.find({ name: { $regex: courseName, $options: 'i' } })
    .then(courses => {
      if (courses.length === 0) {
        return response.status(404).send('Course not found');
      }
      return response.send(courses);
    })
    .catch(error => {
      console.error(error);
      return response.status(500).send('Search error');
    });
};

