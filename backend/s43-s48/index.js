
// server variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config(); 
const port = process.env.PORT || 4000;
const userRoutes = require('./routes/userRoutes.js');
const courseRoutes = require('./routes/courseRoutes.js');
const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); //allows our hosted front-end app to send requests to this server

// Routes
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);


// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gomez.k6bt02u.mongodb.net/B303-Booking-API?retryWrites=true&w=majority`,
	{useNewUrlParser: true, useUnifiedTopology: true});

mongoose.connection.on('error', () => console.log("Can't connect to the database"));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));

app.listen(port, () => {
console.log(`Booking System API is now running at localhost:${port}`);
});



module.exports = app;