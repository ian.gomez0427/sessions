const jwt = require('jsonwebtoken');
const secret_key = "KapugeGaNare0427";

// generating a token
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	// the jwt.sign(generates a token using user_data and secret_key. The 3rd argument serves as additional option
	return jwt.sign(user_data, secret_key, {});
}

// verifying a Token
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (typeof token === 'undefined'){
		return response.send({auth: "User token is required!"})
	}
	// console.log(token);

	// removes the default 'Bearer' from the token in the authorization header
	token = token.slice(7, token.length);

	// console.log(token)

	jwt.verify(token, secret_key, (error, decoded_token) => {

		if (error) {
      		return response.status(401).json({
        	auth: "Token authentication failure!",
        	message: error.message
      		});
    	}

		// console.log(decoded_token)
		request.user = decoded_token; 

		next();
	});
};

// verifying if user is admin
module.exports.verifyAdmin = (request, response, next) => {
	if(request.user.isAdmin){
		return next();
	}
	return response.status(403).json({
    auth: "Failed",
    message: "Admin access is required."
  });
};

