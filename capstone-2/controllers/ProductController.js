const Product = require("../models/Product.js");

module.exports.addProduct = (request, response) => {
  // Check if a product with the same item already exists
  return Product.findOne({ item: request.body.item })
    .then(existingProduct => {
      if (existingProduct) {
        return { error: 'Product item already exists.' };
      }

      // If no existing product with the same item, create a new product
      let newProduct = new Product({
        item: request.body.item,
        description: request.body.description,
        price: request.body.price,
        availableItems: request.body.availableItems,
        createdBy: request.user.email
      });

      return newProduct.save()
        .then(savedProduct => {
          return { message: 'Product added successfully!', data: savedProduct };
        })
        .catch(error => {
          return { error: 'An error occurred while adding the product.' };
        });
    })
    .catch(error => {
      return { error: 'An error occurred while checking for existing product.' };
    });
};

module.exports.getAllProducts = () => {
  return Product.find()
    .then(products => {
      return products;
    })
    .catch(error => {
      return { error: 'An error occurred while retrieving products.' };
    });
};

module.exports.getAllAvailableProducts = () => {
  return Product.find({ availableItems: { $gt: 0 } })
    .then(products => {
      return products;
    })
    .catch(error => {
      return { error: 'An error occurred while retrieving available products.' };
    });
};

module.exports.getNotAvailableProducts = () => {
  return Product.find({ availableItems: 0 })
    .then(products => {
      return products;
    })
    .catch(error => {
      return { error: 'An error occurred while retrieving available products.' };
    });
};

module.exports.getProductById = (productId) => {
  return Product.findById(productId)
    .then(product => {
      if (!product) {
        return { message: 'Product ID not found.' };
      }
      return { data: product };
    })
    .catch(error => {
      return { error: 'Product ID not found.' };
    });
};

module.exports.getProductByItem = (item) => {
  return Product.findOne({ item })
    .then(product => {
      if (!product) {
        return { message: 'Product item not found.' };
      }
      return { data: product };
    })
    .catch(error => {
      return { error: 'Product item not found.' };
    });
};

module.exports.updateProduct = (_id, updatedData) => {
  return Product.findById(_id)
    .then(product => {
      if (!product) {
        return { message: 'Product ID not found.' };
      }

      // Update product properties with the provided updated data
      if (updatedData.item) product.item = updatedData.item;
      if (updatedData.description) product.description = updatedData.description;
      if (updatedData.price) product.price = updatedData.price;
      if (updatedData.availableItems) product.availableItems = updatedData.availableItems;

      return product.save()
        .then(updatedProduct => {
          return { message: `${updatedData.item} has been updated successfully`, data: updatedProduct };
        })
        .catch(error => {
          return { error: 'An error occurred while updating the product.' };
        });
    })
    .catch(error => {
      return { error: 'An error occurred while updating the product.' };
    });
};

module.exports.archiveProduct = (productId) => {
  return Product.findById(productId)
    .then(product => {
      if (!product) {
        return { message: 'Product ID not found.' };
      }

      // Set availableItems to 0 to archive the product
      product.availableItems = 0;

      return product.save()
        .then(archivedProduct => {
          return { message: `${archivedProduct.item} has been archived successfully`, data: archivedProduct };
        })
        .catch(error => {
          return { error: 'An error occurred while archiving the product.' };
        });
    })
    .catch(error => {
      return { error: 'An error occurred while archiving the product.' };
    });
};


// module.exports.getAllCourses = (request, response) => {
//   return Course.find({}).then(result => {
//     return response.send(result);
//   })
// }

// module.exports.getAllActiveCourses = (request, response) => {
//   return Course.find({isActive: true}).then (result => {
//     return response.send(result);
//   })
// }

// module.exports.getCourse = (request, response) => {
//   return Course.findById(request.params.id).then(result => {

//     return response.send(result);
//   }).catch(error => {
//             console.error(error);
//             // return response.status(500).json({ error: 'Course not found' });
//             return response.status(500).send('Course not found');
//         });

// }

// module.exports.updateCourse = (request, response) => {
//   let updated_course_details = {
//     name: request.body.name,
//     description: request.body.description,
//     price: request.body.price
//   };

//   return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course,error) => {
//     if (error){
//       return response.send({
//         message: error.message
//       })
//     }
//     return response.send({
//       message: 'Course has been updated successfully!'
//     })
//   })
// }

// module.exports.archiveCourse = (request, response) => {
//     return Course.findByIdAndUpdate(request.params.id, { isActive: 'false' }).then((course,error) => {
//     if (error){
//       return response.send({
//       message: 'Failed to Archive!'
//     })
//     }
//     return response.send({
//       message: 'Course has been archived successfully!'
//     })
//   })
// }

// module.exports.activateCourse = (request, response) => {
//     return Course.findByIdAndUpdate(request.params.id, { isActive: 'true' }).then((course,error) => {
//     if (error){
//       return response.send({
//       message: 'Failed to Activate!'
//     })
//     }
//     return response.send({
//       message: 'Course has been activated successfully!'
//     })
//   })
// }


// //////////////////////////
// module.exports.searchCoursesByName = (request, response) => {
//   const { courseName } = request.body;
  
//   Course.find({ name: { $regex: courseName, $options: 'i' } })
//     .then(courses => {
//       if (courses.length === 0) {
//         return response.status(404).send('Course not found');
//       }
//       return response.send(courses);
//     })
//     .catch(error => {
//       console.error(error);
//       return response.status(500).send('Search error');
//     });
// };

