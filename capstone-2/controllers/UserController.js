const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.registerUser = (requestBody) => {
    return User.findOne({ email: requestBody.email })
      .then(existingUser => {
        if (existingUser && existingUser.email === requestBody.email) {
          return { message: 'Email already registered!' };
        }

        const newUser = new User({
          email: requestBody.email,
          password: bcrypt.hashSync(requestBody.password, 10),
          isAdmin: false
        });

        return newUser.save()
          .then(registeredUser => {
            return {
              message: 'Successfully registered a user!',
              data: registeredUser
            };
          })
          .catch(error => {
            return { error: 'An error occurred while registering the user.' };
          });
      })
      .catch(error => {
        return { error: 'An error occurred while registering the user.' };
      });
};

module.exports.loginUser = (requestBody) => {
    return User.findOne({ email: requestBody.email })
      .then(user => {
        if (!user) {
          return { message: 'Cannot find the email address. Please register!' };
        }
        const isPasswordCorrect = bcrypt.compareSync(requestBody.password, user.password);

        if (isPasswordCorrect) {
          const accessToken = auth.createAccessToken(user);
          return { accessToken };
        } else {
          return { message: 'Please check your username and password combination.' };
        }
      })
      .catch(error => {
        return { error: 'An error occurred while logging in.' };
      });
};

module.exports.getAllUsers = () => {
    return User.find({ isAdmin: false })
      .then(users => {
        return users;
      })
      .catch(error => {
        return { error: 'An error occurred while retrieving users.' };
      });
};

module.exports.getAllAdminUser = () => {
    return User.find({ isAdmin: true })
      .then(adminUsers => {
        return adminUsers;
      })
      .catch(error => {
        return { error: 'An error occurred while retrieving admin users.' };
      });
};

module.exports.getUserDetails = (requestBody) => {
    return User.findOne({ email: requestBody.email })
      .then(user => {
        if (!user) {
          return { message: 'User not found.' };
        }

        user.password = ''; // Hide the password
        return user;
      })
      .catch(error => {
        return { error: 'An error occurred while retrieving user details.' };
      });
};

module.exports.setAdminAccessByEmail = (email, isAdmin) => {
  return User.findOne({ email })
    .then(user => {
      if (!user) {
        return { message: 'User not found.' };
      }

      user.isAdmin = isAdmin;
      return user.save()
        .then(() => {
          return { message: `User has been set as ${isAdmin ? 'admin' : 'non-admin'}.` };
        })
        .catch(error => {
          return { error: 'An error occurred while updating user access.' };
        });
    })
    .catch(error => {
      return { error: 'An error occurred while updating user access.' };
    });
};


/*

module.exports = {

  async registerUser(requestBody) {
    try {
      const existingUser = await User.findOne({ email: requestBody.email });

      if (existingUser && existingUser.email === requestBody.email) {
        return { message: 'Email already registered!' };
      }

      const newUser = new User({
        email: requestBody.email,
        password: bcrypt.hashSync(requestBody.password, 10),
        isAdmin: false
      });

      const registeredUser = await newUser.save();
      return {
        message: 'Successfully registered a user!',
        data: registeredUser
      };
    } catch (error) {
      return { error: 'An error occurred while registering the user.' };
    }
  },

  // async loginUser(requestBody) {
  //   try {
  //   	console.log('Request Body:', requestBody); 
  //     const user = await User.findOne({ email: requestBody.email });

  //     if (!user) {
  //       return { message: 'Cannot find the email address. Please register!' };
  //     }

  //     const isPasswordCorrect = bcrypt.compareSync(requestBody.password, user.password);

  //     if (isPasswordCorrect) {
  //       const accessToken = auth.createAccessToken(user);
  //       return { accessToken };
  //     } else {
  //       return { message: 'Please check your username and password combination.' };
  //     }
  //   } catch (error) {
  //     return { error: 'An error occurred while logging in.' };
  //   }
  // },

  async getAllUsers() {
    try {
      const users = await User.find({ isAdmin: false });
      return users;
    } catch (error) {
      return { error: 'An error occurred while retrieving users.' };
    }
  },

  async getAllAdminUser() {
    try {
      const adminUsers = await User.find({ isAdmin: true });
      return adminUsers;
    } catch (error) {
      return { error: 'An error occurred while retrieving admin users.' };
    }
  },

  async getUserDetails(requestBody) {
    try {
      const user = await User.findOne({ _id: requestBody.id });
      if (!user) {
        return { message: 'User not found.' };
      }

      user.password = ''; // Hide the password
      return user;
    } catch (error) {
      return { error: 'An error occurred while retrieving user details.' };
    }
  },

  async setUserAsAdmin(userId) {
    try {
      const user = await User.findById(userId);
      if (!user) {
        return { message: 'User not found.' };
      }

      user.isAdmin = true;
      await user.save();

      return { message: 'User has been set as admin.' };
    } catch (error) {
      return { error: 'An error occurred while setting user as admin.' };
    }
  }
  // Other controller methods for login, getUserById, setUserAsAdmin, etc.
};

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// checks if a user is found with an existing email
		if (result == null){
			return response.send({
				message: 'Cannot find the email address. Please check the email address or Register!.'
			})
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Please check your username and password combination'
			})
		}
	}).catch(error => response.send(error));
}

  async loginUser(requestBody) {
    try {
    	console.log('Request Body:', requestBody); 
      const user = await User.findOne({ email: requestBody.email });

      if (!user) {
        return { message: 'Cannot find the email address. Please register!' };
      }

      const isPasswordCorrect = bcrypt.compareSync(requestBody.password, user.password);

      if (isPasswordCorrect) {
        const accessToken = auth.createAccessToken(user);
        return { accessToken };
      } else {
        return { message: 'Please check your username and password combination.' };
      }
    } catch (error) {
      return { error: 'An error occurred while logging in.' };
    }
  },

/*
module.exports.checkEmailExists = (request_body) =>{
	// insert logic here
	return User.find({email: request_body.email}).then((result, error) => {
		if (error){
			return{
				message: error.message
			}
		}

		if (result.length <= 0){
			return false;
		}
		// this will only return true if there are no errors AND there is an existing user email from the database
		return true;
	})

}

module.exports.getAllUsers = (request, response) => {
  return User.find({isAdmin: false}).then(result => {
    return response.send(result);
  })
}

module.exports.getAllAdminUser = (request, response) => {
  return User.find({isAdmin: true}).then (result => {
    return response.send(result);
  })
}

// module.exports.registerUser = (request_body) => {
// 	let new_user = new User ({
// 		firstName: request_body.firstName,
// 		lastName: request_body.lastName,
// 		email: request_body.email,
// 		mobileNo: request_body.mobileNo,
// 		password: bcrypt.hashSync(request_body.password, 10)
// 	});
// 	return new_user.save().then((registered_user, error) =>{
// 		if(error){
// 			return {
// 				message: error.message
// 			};
// 		}
// 		return {
// 			message: 'Successfully registered a user!',
// 			data: registered_user
// 		};
// 	}).catch(error => console.log(error));
// }

module.exports.registerUser = (request_body) => {
	return User.findOne({email: request_body.email}).then((result, error) => {
		// checks if task already exist
		if (result != null && result.email == request_body.email) {
			return {message: "Email already registered!"};
		}
		 else {		
			let new_user = new User ({
				firstName: request_body.firstName,
				lastName: request_body.lastName,
				email: request_body.email,
				mobileNo: request_body.mobileNo,
				password: bcrypt.hashSync(request_body.password, 10)
			});
			
			return new_user.save().then((registered_user, error) =>{
				if(error){
					return {
						message: error.message
					};
				}
				return {
					message: 'Successfully registered a user!',
					data: registered_user
				};
			}).catch(error => console.log(error));
		}
})
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// checks if a user is found with an existing email
		if (result == null){
			return response.send({
				message: 'The user is not registered yet.'
			})
		}

		// if a user is found with an existing email, then check if the password of that user matches the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Please check your username and password combination'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";
		return user;
	}).catch(error => console.log(error));
}



module.exports.enroll = async (request, response) => {
	if (request.user.isAdmin){
		return response.send("Action Forbidden");
	}

	// [SECTION] Updating User Collection
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById (request.user.id).then(user => {
		let new_enrollment = {
			courseId: request.body.courseId
			
		}
		user.enrollments.push (new_enrollment);
		return user.save().then(updated_user => true).catch (error => error.message);
	})

	if(isUserUpdated !== true) {
		return response.send({message: isUserUpdated});
	}

	// [SECTION] Updating Course Collection
	let isCourseUpdated = await Course.findById (request.body.courseId).then (course => {
		let new_enrollee = {
			userId: request.user.id,
			name: request.user.lastName
		}
		course.enrollees.push(new_enrollee);
		return course.save().then(updated_course => true).catch(error => error.message)

	})

	if (isCourseUpdated !== true){
		return response.send ({message: isCourseUpdated});
	}

	// [SECTION] Once isUserUpdated AND isCourseUpdated, return true
	if (isUserUpdated && isCourseUpdated){
		return response.send ({message: 'Enrolled successfully!'});
	}

}

module.exports.getEnrollments = (request, response) => {
	User.findById(request.user.id)
		.then(user => response.send(user.enrollments))
		.catch(error => response.send(error.message))
}

*/