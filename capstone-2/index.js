
// server variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config(); 
const port = process.env.PORT || 4000;

const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js'); 
const orderRoutes = require('./routes/orderRoutes.js');

const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); //allows our hosted front-end app to send requests to this server

// Routes
app.use('/ecommerce/users', userRoutes);
app.use('/ecommerce/products', productRoutes);
// app.use('/ecommerce/orders', orderRoutes);


// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gomez.k6bt02u.mongodb.net/E-commerce-API-Gomez?retryWrites=true&w=majority`,
	{useNewUrlParser: true, useUnifiedTopology: true});

mongoose.connection.on('error', () => console.log("Can't connect to the database"));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));

app.listen(port, () => {
console.log(`E-commerce-API-Gomez is now running at localhost:${port}`);
});


module.exports = app;