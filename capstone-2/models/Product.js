const mongoose = require('mongoose');

// Product Schema
const productSchema = new mongoose.Schema({

  item: {
    type: String,
    required: true,
    unique: true
  },

  description: {
    type: String,
    required: true
  },

  price: {
    type: Number,
    required: true
  },

  availableItems: {
    type: Number,
    required: true,
    default: 0 
  },

  createdOn: {
    type: Date,
    default: Date.now
  },

  createdBy: {
    type: String, 
  },

});

module.exports = mongoose.model("Product", productSchema);


