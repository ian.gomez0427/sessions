const express = require('express')
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');

// Add Product
router.post('/add', auth.verify, auth.verifyAdmin, (req, res) => {
  ProductController.addProduct(req, res)
    .then(result => {
      res.send(result); // Sending the result returned by the controller
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while adding the product.' });
    });
});

// Get All Products
router.get('/allproducts', (req, res) => {
  ProductController.getAllProducts()
    .then(products => {
      res.send(products);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving products.' });
    });
});

// get all available products
router.get('/available', (req, res) => {
  ProductController.getAllAvailableProducts()
  	.then(products => {
      res.send(products);
  })
  	.catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving available products.' });
    });
})

// get all archived products
router.get('/nostock', (req, res) => {
  ProductController.getNotAvailableProducts()
  	.then(products => {
      res.send(products);
  })
  	.catch(error => {
  		console.error('Error retrieving available products:', error);
      res.status(500).json({ error: 'An error occurred while retrieving available products.' });
    });
})

// (Admin) Get Product by ID
router.get('/find/:productId', (req, res) => {
  const productId = req.params.productId;
  ProductController.getProductById(productId)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving the product.' });
    });
});

// Get Product by Item
router.post('/item', (req, res) => {
  const { item } = req.body;
  ProductController.getProductByItem(item)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving the product.' });
    });
});

// (Admin )Update Product
router.put('/updateProduct/:_id', auth.verify, auth.verifyAdmin, (req, res) => {
  ProductController.updateProduct(req.params._id, req.body)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while updating the product.' });
    });
});

// (Admin) Archive Product
router.put('/archive/:productId', auth.verify, auth.verifyAdmin, (req, res) => {
  ProductController.archiveProduct(req.params.productId)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while archiving the product.' });
    });
});

module.exports = router;


// // You can destructure the 'auth' variable to extract the function being exported from it. You can then use the functions directly without having to use dot (.) notation.
// // const {verify, verifyAdmin} = auth;

// // Insert routes here

// // Create single post
// router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.addCourse(request, response)
// });


// // Get all courses
// router.get('/all', (request, response) => {
//   CourseController.getAllCourses (request, response);
// })

// // get all active courses
// router.get('/', (request, response) => {
//   CourseController.getAllActiveCourses(request, response);
// })

// // get single course
// router.get('/:id', (request, response) => {
//   CourseController.getCourse(request, response);
// })

// // update single course
// router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.updateCourse(request, response);
// })

// // archive single course
// router.put('/:id/archive', auth.verify, (request, response) => {
//   CourseController.archiveCourse(request, response);
// })

// // activate single course
// router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.activateCourse(request, response);
// })

// // find Course by name
// router.post('/search', auth.verify, (request, response) => {
//   CourseController.searchCoursesByName(request, response);
// })



// module.exports = router;