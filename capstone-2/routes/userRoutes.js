const express = require('express');
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');
const router = express.Router();


// Register User
router.post('/register', async (req, res) => {
  const result = await UserController.registerUser(req.body);
  res.send(result);
});

//Login user
router.post('/login', async (req, res) => {
  const result = await UserController.loginUser(req.body);
  res.send(result);
});

// (Admin) get all users
router.get('/alluser', async (req, res) => {
  const users = await UserController.getAllUsers();
  res.send(users);
});

// (Admin) get all admin users
router.get('/alladmin', async (req, res) => {
  const adminUsers = await UserController.getAllAdminUser();
  res.send(adminUsers);
});

// (Admin) Get user details
router.post('/details', auth.verify, auth.verifyAdmin, async (req, res) => {
  const result = await UserController.getUserDetails(req.body);
  res.send(result);
});

// (Admin) Change Admin access
router.put('/set-admin', auth.verify, auth.verifyAdmin, (req, res) => {
  const { email, isAdmin } = req.body; // Assuming isAdmin is a boolean value in the request body
  UserController.setAdminAccessByEmail(email, isAdmin)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while setting user access.' });
    });
});

module.exports = router; 

/*
// Check if email exists
router.post('/check-email', (request, response) =>{
	// controller function goes here
	UserController.checkEmailExists(request.body).then((result => {
		response.send(result);
	}));
})

// Register User
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})

})

//Login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Get user details
router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

// Enroll user to a course
router.post('/enroll', auth.verify, (request, response) =>{
	UserController.enroll(request, response);
})

router.get('/allUser', (request, response) => {
  UserController.getAllUsers (request, response);
})

router.get('/allAdmin', (request, response) => {
  UserController.getAllAdminUser(request, response);
})

// get user enrollments
router.get('/enrollments', auth.verify, (request, response) =>{
	UserController.getEnrollments(request, response);
})

*/
module.exports = router; 