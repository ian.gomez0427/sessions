import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import {Container} from 'react-bootstrap';

function App() {
  return (
    <> 
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>
    
  );
}

export default App;
