import {Button, Row, Col} from 'react-bootstrap';

export default function Banner () {
	return (
			<Row>	
			<Col className = "p-5 text-center">
				<h1>Zuitt Coding</h1>
				<h3>Become a web developer in 2 months</h3>
				<p> Staying at home can be an opportunity to get your dream job in Tech, with the No. 1 Coding Bootcamp in the Philippines.!</p>

				<Button variant = "primary"> Apply Now!</Button>
			</Col>
			</Row>	
		)
}