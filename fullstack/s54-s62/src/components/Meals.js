import { Card, Row, Col, Button } from 'react-bootstrap';

export default function Meals() {
  return (
    <Row className="my-5">
      <Col xs={12} md={6}>
        <Card className="cardHighlight p-3 border-0">
          <Card.Body>
            <Card.Title>
              <h2>Kilig Dinuguan</h2>
            </Card.Title>
            <Card.Text>
              <h5>Description:</h5>
              Indulge in the rich and flavorful tradition of Filipino cuisine with our signature Dinuguan. This classic dish features tender cuts of pork simmered to perfection in a velvety sauce crafted from pork blood, vinegar, and aromatic spices. The result is a harmonious blend of savory and tangy flavors, presented in a deep, inviting hue. Served with steamed rice or traditional rice cakes, our Dinuguan promises a delightful culinary adventure that captures the essence of Filipino comfort food.
            </Card.Text>
            <Card.Text>
              <h5>Price:</h5>
              P 99.00
            </Card.Text>
            <Button variant="primary">Order Now!</Button>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={6}>
        <Card className="cardHighlight p-3 border-0">
          <Card.Img variant="top" src='dinuguan.png' />
        </Card>
      </Col>

      <Col xs={12} md={6}>
        <Card className="cardHighlight p-3 border-0">
          <Card.Body>
            <Card.Title>
              <h2>Kilig Caldereta</h2>
            </Card.Title>
            <Card.Text>
              <h5>Description:</h5>
              Experience the warmth of Filipino hospitality on your plate with our mouthwatering Caldereta. This hearty dish showcases succulent cuts of meat, often beef or goat, slow-cooked to tenderness in a rich tomato-based sauce. Bursting with the flavors of bell peppers, onions, and a medley of aromatic spices, our Caldereta is a true celebration of bold and comforting Filipino flavors. Savor it with a side of steamed rice for a satisfying culinary journey that embodies the heart and soul of Filipino cuisine.
            </Card.Text>
            <Card.Text>
              <h5>Price:</h5>
              P 99.00
            </Card.Text>
            <Button variant="primary">Order Now!</Button>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={6}>
        <Card className="cardHighlight p-3 border-0">
          <Card.Img variant="top" src='kaldereta.png' />
        </Card>
      </Col>
    </Row>
  );
}
